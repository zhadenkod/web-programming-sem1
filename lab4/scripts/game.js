window.onload = function () {
    let srcWhiteChecker = "../images/white-checker.png";
    let srcWhiteKing = "../images/white-king.png";
    let srcBlackChecker = "../images/black-checker.png";
    let srcBlackKing = "../images/black-king.png";

    // 1 - black checker
    // 2 - white checker
    // 3 - black king
    // 4 - white king
    let gameBoard = [
        [0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [2, 0, 2, 0, 2, 0, 2, 0],
        [0, 2, 0, 2, 0, 2, 0, 2],
        [2, 0, 2, 0, 2, 0, 2, 0]
    ];
    let exampleBoard = [
            [0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 1],
            [0, 0, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 2],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 3, 0, 0, 0, 0, 0]
    ];

    let boardModel = [];
    for (let r = 0; r < 8; r++) {
        boardModel.push([]);
    }
    for (let r = 0; r < 8; r++) {
        boardModel[r].push(null);
    }

    let helpMode = {
        on: false,
        row: null,
        column: null
    };

    let startGame = () => {
        clearBoardModel();
        makeBoardModel(gameBoard);
        updateBoard();
    }

    let startExample = () => {
        clearBoardModel();
        makeBoardModel(exampleBoard);
        updateBoard();
    }

    let updateBoard = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                let boardElement = document.getElementById(`${row}${column}`);
                if (boardElement !== undefined) {
                    let child = boardElement.children[0];
                    if (child !== undefined && child !== null) {
                        boardElement.removeChild(child);
                    }
                }
                let checker = boardModel[row][column];

                if (checker !== null) {
                    let image = document.createElement("img");
                    image.className = "checker";
                    image.onclick = () => {
                        onClickChecker(checker);
                    }
                    if (checker.playerColor === "white") {
                        if (checker.isKing) {
                            image.src = srcWhiteKing;
                        } else {
                            image.src = srcWhiteChecker;
                        }
                    }
                    if (checker.playerColor === "black") {
                        if (checker.isKing) {
                            image.src = srcBlackKing;
                        } else {
                            image.src = srcBlackChecker;
                        }
                    }
                    boardElement.append(image);
                }
            }
        }
    }

    let onClickChecker = (checker) => {
        let element = document.getElementById(`${checker.row}${checker.column}`).children[0];
        if (helpMode.on) {
            if (isHelpMode(checker)) {
                element.className = "checker";
                turnOffHelp();
            }
        } else {
            deactivateCheckers();
            element.className = "checker active-checker";
            turnOnHelp(checker);
        }
    }

    let deactivateCheckers = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                let element = document.getElementById(`${row}${column}`).children[0];
                if (element !== undefined) {
                    element.className = "checker";
                }
            }
        }
    }

    let isHelpMode = (checker) => {
        return helpMode.row === checker.row && helpMode.column === checker.column;
    }

    let turnOffHelp = () => {
        helpMode.on = false;
        helpMode.row = null;
        helpMode.column = null;
        clearBoardCells();
    }

    let turnOnHelp = (checker) => {
        helpMode.on = true;
        helpMode.row = checker.row;
        helpMode.column = checker.column;
        let possiblePositions = getPossiblePositions(checker);
        let killingPositions = getKillPositions(checker);
        clearBoardCells();
        if (killingPositions.length !== 0) {
            for (let i in killingPositions) {
                let position = killingPositions[i];
                document.getElementById(`${position.row}${position.column}`).className = "kill-cell";
            }
        } else {
            for (let i in possiblePositions) {
                let position = possiblePositions[i];
                document.getElementById(`${position.row}${position.column}`).className = "possible-cell";
            }
        }
    }

    let clearBoardCells = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                if ((row + column) % 2 === 1) {
                    document.getElementById(`${row}${column}`).className = "black";
                }
            }
        }
    }

    let getPossiblePositions = (checker) => {
        let direction = (checker.playerColor === "black") ? 1 : -1;
        let positions = [];
        for (let rowDir = -1; rowDir <= 1; rowDir += 2) {
            for (let columnDir = -1; columnDir <= 1; columnDir += 2) {
                if (checker.isKing) {
                    for (let distance = 1; distance < 8; distance++) {
                        let row = Number(checker.row) + rowDir * distance;
                        let column = Number(checker.column) + columnDir * distance;
                        if (isPositionFree(row, column)) {
                            positions.push({row: row, column: column});
                        } else {
                            break;
                        }
                    }
                } else {
                    if (rowDir !== direction) {
                        continue;
                    }
                    let row = Number(checker.row) + rowDir;
                    let column = Number(checker.column) + columnDir;
                    if (isPositionFree(row, column)) {
                        positions.push({row: row, column: column})
                    }
                }
            }
        }
        return positions;
    }

    let getKillPositions = (checker) => {
        let positions = [];
        for (let rowDir = -1; rowDir <= 1; rowDir += 2) {
            for (let columnDir = -1; columnDir <= 1; columnDir += 2) {
                if (checker.isKing) {
                    for (let distance = 1; distance < 8; distance++) {
                        let victim = getChecker(Number(checker.row)+ rowDir * distance,
                            Number(checker.column) + columnDir * distance);
                        if (victim !== null && victim.playerColor !== checker.playerColor) {
                            for (let moveDistance = distance + 1; moveDistance < 8; moveDistance++) {
                                let row = Number(checker.row) + rowDir * moveDistance;
                                let column = Number(checker.column) + columnDir * moveDistance;
                                if (isPositionFree(row, column)) {
                                    positions.push({row: row, column: column});
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    let victim = getChecker(Number(checker.row) + rowDir, Number(checker.column) + columnDir);
                    if (victim !== null && victim.playerColor !== checker.playerColor) {
                        let row = Number(checker.row) + rowDir * 2;
                        let column = Number(checker.column) + columnDir * 2;
                        if (isPositionFree(row, column)) {
                            positions.push({row: row, column: column});
                        }
                    }
                }
            }
        }
        return positions;
    }

    let getChecker = (row, column) => {
        if (row < 0 || row >= 8 || column < 0 || column >= 8) {
            return null;
        }
        return boardModel[row][column];
    }

    let isPositionFree = (row, column) => {
        if (row < 0 || row >= 8 || column < 0 || column >= 8) {
            return false;
        }
        return boardModel[row][column] === null;
    }

    let makeBoardModel = (board) => {
        for (let row in board) {
            for (let column in board[row]) {
                let checker = null;
                switch (board[row][column]) {
                    case 1:
                        checker = Checker(row, column, "black", false);
                        break;
                    case 2:
                        checker = Checker(row, column, "white", false);
                        break;
                    case 3:
                        checker = Checker(row, column, "black", true);
                        break;
                    case 4:
                        checker = Checker(row, column, "white", true);
                        break;
                }
                if (checker != null) {
                    boardModel[row][column] = checker;
                }
            }
        }
    }

    let Checker = (row, column, playerColor, isKing) => {
        return {
            row: row,
            column: column,
            playerColor: playerColor,
            isKing: isKing
        }
    }

    let clearBoardModel = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                boardModel[row][column] = null;
            }
        }
        clearBoardCells();
    }

    document.getElementById("game-button").onclick = startGame;
    document.getElementById("example1-button").onclick = startExample;

    startGame();
}