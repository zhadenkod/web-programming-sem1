package ru.omsu.imit.course4;

public class Main {

    public static void main (String[] args) {
        SocketClient socketClient = new SocketClient();
        socketClient.getPage("htmlbook.ru");

        URLConnectionClient urlConnectionClient = new URLConnectionClient();
        urlConnectionClient.getPage("https://omsu.ru/");
    }

}
