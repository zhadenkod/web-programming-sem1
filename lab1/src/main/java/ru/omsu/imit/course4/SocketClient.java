package ru.omsu.imit.course4;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class SocketClient implements IClient {
    private final int port;

    public SocketClient() {
        port = 80;
    }

    public void getPage(String url) {
        BufferedReader reader;
        File file = new File("socket_file");
        try (FileWriter writer = new FileWriter(file)) {
            Socket socket = new Socket(url, port);
            String request = "GET / HTTP/1.1\r\nConnection: close\r\nHost:" + url + "\r\n\r\n";

            final OutputStream outputStream = socket.getOutputStream();
            outputStream.write(request.getBytes(StandardCharsets.US_ASCII));

            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line;
            boolean pageContent = false;
            while ((line = reader.readLine()) != null) {
                if (line.equals("") || line.equals("\n")) {
                    pageContent = true;
                }
                if (pageContent) {
                    writer.write(line);
                } else {
                    System.out.println(line);
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
