package ru.omsu.imit.course4;

import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

public class URLConnectionClient implements IClient {
    private void printHeaders(Map<String, List<String>> headers) {
        for (String header : headers.keySet()) {
            System.out.println(header + ": " + headers.get(header));
        }
    }

    @Override
    public void getPage(String url) {
        BufferedReader reader;
        File file = new File("urlconnection_file");
        try (FileWriter writer = new FileWriter(file)) {
            if (url.startsWith("https")) {
                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null; }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) { }
                    public void checkServerTrusted(X509Certificate[] certs, String authType) { }

                } };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) { return true; }
                };

                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                URL connectionURL = new URL(url);
                HttpsURLConnection connection = (HttpsURLConnection) connectionURL.openConnection();
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                printHeaders(connection.getHeaderFields());
            } else {
                URL connectionURL = new URL(url);
                URLConnection connection = connectionURL.openConnection();
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }

            String line;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
            }
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
            System.out.println(e.getMessage());
        }
    }
}
