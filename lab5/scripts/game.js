window.onload = function () {
    let srcWhiteChecker = "../images/white-checker.png";
    let srcWhiteKing = "../images/white-king.png";
    let srcBlackChecker = "../images/black-checker.png";
    let srcBlackKing = "../images/black-king.png";

    let moveType = "-";
    let killType = ":";

    let translate = (columnNumber) => {
        switch (columnNumber) {
            case 0:
                return 'a';
            case 1:
                return 'b';
            case 2:
                return 'c';
            case 3:
                return 'd';
            case 4:
                return 'e';
            case 5:
                return 'f';
            case 6:
                return 'g';
            case 7:
                return 'h';
        }
    }

    // 1 - black checker
    // 2 - white checker
    // 3 - black king
    // 4 - white king
    let gameBoard = [
        [0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0, 1, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [2, 0, 2, 0, 2, 0, 2, 0],
        [0, 2, 0, 2, 0, 2, 0, 2],
        [2, 0, 2, 0, 2, 0, 2, 0]
    ];
    let exampleBoard = [
            [0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 1],
            [0, 0, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 2],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 3, 0, 0, 0, 0, 0]
    ];

    let boardShot;
    let turn = "white";
    let moveRecord = {
        startPosition: "",
        moveType: "",
        endPosition: ""
    };

    let boardModel = [];
    for (let r = 0; r < 8; r++) {
        boardModel.push([]);
    }
    for (let r = 0; r < 8; r++) {
        boardModel[r].push(null);
    }

    let helpMode = {
        on: false,
        row: null,
        column: null
    };

    let startGame = () => {
        clearBoardModel();
        clearMoveRecord();
        hideMoveButtons();
        turnOffHelp();
        makeBoardModel(gameBoard);
        updateBoard();
        setWhiteTurn();
    }

    let startExample = () => {
        clearBoardModel();
        clearMoveRecord();
        hideMoveButtons();
        turnOffHelp();
        makeBoardModel(exampleBoard);
        updateBoard();
        setWhiteTurn();
    }

    let updateBoard = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                let boardElement = document.getElementById(`${row}${column}`);
                if (boardElement !== undefined) {
                    let child = boardElement.children[0];
                    if (child !== undefined && child !== null) {
                        boardElement.removeChild(child);
                    }
                }
                let checker = boardModel[row][column];
                if (checker !== null) {
                    let image = document.createElement("img");
                    image.className = "checker";
                    image.onclick = () => {
                        onClickChecker(checker);
                    }
                    if (checker.playerColor === "white") {
                        if (checker.isKing) {
                            image.src = srcWhiteKing;
                        } else {
                            image.src = srcWhiteChecker;
                        }
                    }
                    if (checker.playerColor === "black") {
                        if (checker.isKing) {
                            image.src = srcBlackKing;
                        } else {
                            image.src = srcBlackChecker;
                        }
                    }
                    boardElement.append(image);
                }
            }
        }
    }

    let onClickChecker = (checker) => {
        boardShot = JSON.parse(JSON.stringify(boardModel));
        let element = document.getElementById(`${checker.row}${checker.column}`).children[0];
        if (checker.playerColor === turn) {
            if (helpMode.on) {
                if (isHelpMode(checker)) {
                    element.className = "checker";
                    turnOffHelp();
                }
            } else {
                deactivateCheckers();
                element.className = "checker active-checker";
                turnOnHelp(checker);
                moveRecord.startPosition = `${8 - Number(checker.row)}${translate(Number(checker.column))}`;
            }
        }
    }

    let onClickCell = (position, checker, positionStatus) => {
        boardModel[checker.row][checker.column] = null;
        if (positionStatus === "kill") {
            moveRecord.moveType = killType;
            if (checker.isKing) {
                let rowDirection, columnDirection;
                if (checker.row > position.row) {
                    rowDirection = -1;
                } else {
                    rowDirection = 1;
                }
                if (checker.column > position.column) {
                    columnDirection = -1;
                } else {
                    columnDirection = 1;
                }
                let row = checker.row + rowDirection, column = checker.column + columnDirection;
                while (row !== position.row && column !== position.column) {
                    if (boardModel[row][column] !== null) {
                        boardModel[row][column] = null;
                    }
                    row += rowDirection;
                    column += columnDirection;
                }
            } else {
                let killedCheckerRow = Number(checker.row) + Number(position.row - checker.row)/2;
                let killedCheckerColumn = Number(checker.column) + Number(position.column - checker.column)/2;
                boardModel[killedCheckerRow][killedCheckerColumn] = null;
            }
        } else {
            moveRecord.moveType = moveType;
        }
        checker.row = position.row;
        checker.column = position.column;
        moveRecord.endPosition = `${8 - Number(checker.row)}${translate(checker.column)}`;
        boardModel[position.row][position.column] = checker;
        if (!checker.isKing && checker.playerColor === "black") {
            if (checker.row === 7) {
                checker.isKing = true;
            }
        } else if (!checker.isKing && checker.playerColor === "white") {
            if (checker.row === 0) {
                checker.isKing = true;
            }
        }
        updateBoard();
        showMoveButtons();
        clearBoardCells();
        if (positionStatus === "kill" && getKillPositions(checker).length !== 0) {
            turnOnHelp(checker);
        }
    }

    let deactivateCheckers = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                let element = document.getElementById(`${row}${column}`).children[0];
                if (element !== undefined) {
                    element.className = "checker";
                }
            }
        }
    }

    let showMoveButtons = () => {
        document.getElementById('move-buttons').className = "right-side__move-buttons-block right-side__move-buttons-block_active";
        document.getElementById('end-button').className = "move-buttons-block__button move-buttons-block__button_active";
        document.getElementById('cancel-button').className = "move-buttons-block__button move-buttons-block__button_active";
    }

    let hideMoveButtons = () => {
        document.getElementById('move-buttons').className = "right-side__move-buttons-block";
        document.getElementById('end-button').className = "move-buttons-block__button";
        document.getElementById('cancel-button').className = "move-buttons-block__button";
    }

    let isHelpMode = (checker) => {
        return helpMode.row === checker.row && helpMode.column === checker.column;
    }

    let turnOffHelp = () => {
        helpMode.on = false;
        helpMode.row = null;
        helpMode.column = null;
        clearBoardCells();
        hideMoveButtons();
    }

    let isThereKillCheckers = (checker) => {
        for (let i = 0; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
                if (i !== checker.row && j !== checker.column) {
                    let cell = boardModel[i][j];
                    if (cell !== null) {
                        if (cell.playerColor === turn) {
                            if (getKillPositions(cell).length !== 0) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    let turnOnHelp = (checker) => {
        helpMode.on = true;
        helpMode.row = checker.row;
        helpMode.column = checker.column;
        let possiblePositions = getPossiblePositions(checker);
        let killingPositions = getKillPositions(checker);
        clearBoardCells();
        if (killingPositions.length !== 0) {
            for (let i in killingPositions) {
                let position = killingPositions[i];
                let cell = document.getElementById(`${position.row}${position.column}`);
                cell.className = "kill-cell";
                cell.onclick = () => {
                    onClickCell(position, checker, "kill");
                }
            }
        } else {
            if (!isThereKillCheckers(checker)) {
                for (let i in possiblePositions) {
                    let position = possiblePositions[i];
                    let cell = document.getElementById(`${position.row}${position.column}`);
                    cell.className = "possible-cell";
                    cell.onclick = () => {
                        onClickCell(position, checker, "possible");
                    }
                }
            }
        }
    }

    let endMove = () => {
        updateBoard();
        writeMove();
        changeTurn();
        turnOffHelp();
        clearBoardCells();
    }

    let cancelMove = () => {
        boardModel = JSON.parse(JSON.stringify(boardShot));
        updateBoard();
        turnOffHelp();
        clearBoardCells();
    }

    let writeMove = () => {
        let record = moveRecord.startPosition + moveRecord.moveType + moveRecord.endPosition;
        let lineBreak = document.createElement("br");
        document.getElementById(`${turn}-moves`).append(record, lineBreak);
    }

    let getPossiblePositions = (checker) => {
        let direction = (checker.playerColor === "black") ? 1 : -1;
        let positions = [];
        for (let rowDir = -1; rowDir <= 1; rowDir += 2) {
            for (let columnDir = -1; columnDir <= 1; columnDir += 2) {
                if (checker.isKing) {
                    for (let distance = 1; distance < 8; distance++) {
                        let row = Number(checker.row) + rowDir * distance;
                        let column = Number(checker.column) + columnDir * distance;
                        if (isPositionFree(row, column)) {
                            positions.push({row: row, column: column});
                        } else {
                            break;
                        }
                    }
                } else {
                    if (rowDir !== direction) {
                        continue;
                    }
                    let row = Number(checker.row) + rowDir;
                    let column = Number(checker.column) + columnDir;
                    if (isPositionFree(row, column)) {
                        positions.push({row: row, column: column})
                    }
                }
            }
        }
        return positions;
    }

    let getKillPositions = (checker) => {
        let positions = [];
        for (let rowDir = -1; rowDir <= 1; rowDir += 2) {
            for (let columnDir = -1; columnDir <= 1; columnDir += 2) {
                if (checker.isKing) {
                    for (let distance = 1; distance < 8; distance++) {
                        let victim = getChecker(Number(checker.row)+ rowDir * distance,
                            Number(checker.column) + columnDir * distance);
                        if (victim !== null && victim.playerColor !== checker.playerColor) {
                            for (let moveDistance = distance + 1; moveDistance < 8; moveDistance++) {
                                let row = Number(checker.row) + rowDir * moveDistance;
                                let column = Number(checker.column) + columnDir * moveDistance;
                                if (isPositionFree(row, column)) {
                                    positions.push({row: row, column: column});
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    let victim = getChecker(Number(checker.row) + rowDir, Number(checker.column) + columnDir);
                    if (victim !== null && victim.playerColor !== checker.playerColor) {
                        let row = Number(checker.row) + rowDir * 2;
                        let column = Number(checker.column) + columnDir * 2;
                        if (isPositionFree(row, column)) {
                            positions.push({row: row, column: column});
                        }
                    }
                }
            }
        }
        return positions;
    }

    let getChecker = (row, column) => {
        if (row < 0 || row >= 8 || column < 0 || column >= 8) {
            return null;
        }
        return boardModel[row][column];
    }

    let isPositionFree = (row, column) => {
        if (row < 0 || row >= 8 || column < 0 || column >= 8) {
            return false;
        }
        return boardModel[row][column] === null;
    }

    let makeBoardModel = (board) => {
        for (let row in board) {
            for (let column in board[row]) {
                let checker = null;
                switch (board[row][column]) {
                    case 1:
                        checker = Checker(row, column, "black", false);
                        break;
                    case 2:
                        checker = Checker(row, column, "white", false);
                        break;
                    case 3:
                        checker = Checker(row, column, "black", true);
                        break;
                    case 4:
                        checker = Checker(row, column, "white", true);
                        break;
                }
                if (checker != null) {
                    boardModel[row][column] = checker;
                }
            }
        }
    }

    let Checker = (row, column, playerColor, isKing) => {
        return {
            row: row,
            column: column,
            playerColor: playerColor,
            isKing: isKing
        }
    }

    let clearBoardCells = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                if ((row + column) % 2 === 1) {
                    let cell = document.getElementById(`${row}${column}`);
                    cell.className = "black";
                    cell.onclick = () => {};
                }
            }
        }
    }

    let clearMoveRecord = () => {
        document.getElementById(`black-moves`).innerHTML = '';
        document.getElementById(`white-moves`).innerHTML = '';
    }

    let clearBoardModel = () => {
        for (let row = 0; row < 8; row++) {
            for (let column = 0; column < 8; column++) {
                boardModel[row][column] = null;
            }
        }
        clearBoardCells();
    }

    let setWhiteTurn = () => {
        turn = "white";
        document.getElementById('white-turn').className = "turn-order__item turn-order__item_active";
        document.getElementById('black-turn').className = "turn-order__item";
    }

    let changeTurn = () => {
        if (turn === "white") {
            turn = "black";
            document.getElementById('black-turn').className = "turn-order__item turn-order__item_active";
            document.getElementById('white-turn').className = "turn-order__item";
        } else if (turn === "black") {
            setWhiteTurn();
        }
    }

    document.getElementById("end-button").onclick = endMove;
    document.getElementById("cancel-button").onclick = cancelMove;
    document.getElementById("game-button").onclick = startGame;
    document.getElementById("example1-button").onclick = startExample;

    startGame();
}